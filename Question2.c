#include<stdio.h>
#include<math.h>
#define PI 3.142

int main()
{
	float radius, area;
	
	printf("Enter the radius of the disc \n");
	scanf("%f" , &radius);
	area = PI * pow(radius, 2);
	printf("Area of a disc = %5.2f\n", area);

return 0;
	
}
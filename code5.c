#include<stdio.h>
int main(){
	int i, j, n;
	printf("Enter the number : ");
	scanf("%d",&n);
	
	for(i=1; i<=n; i++){
		printf("Multiplication table for %d\n",i);
		for(j= 1; j<=12; j++){
			printf("%2d * %2d = %3d\n", i, j, i * j);
		}
		printf("\n");
	}
	return 0;
}